# Anotasi untuk Kepmendikbud 754/P/2020

Posisi persis dari anotasi saya dapat dilihat pada file PDF.

## anotasi 1

> ... harus berpedoman pada indikator kinerja utama (halaman 3)

Artinya: Setiap perguruan tinggi dan LLDIKTI juga harus menargetkan jumlah sitasi (halaman 9). Himbauan seperti ini sudah tidak relevan dengan cara mengukur tingkat rekognisi ([DORA](sfdora.org), [Leiden Manifesto](www.leidenmanifesto.org)).

Pengembangan: Jumlah [sitasi tidak datang dari langit](https://jurnal.ugm.ac.id/bip/article/view/17054). Dengan jumlah jurnal yang sangat banyak serta jumlah artikel yang terbit per hari mungkin bisa ribuan, maka ada komponen lain selain kebaruan yang perlu ditangani, yakni [marketing](https://academic.oup.com/femsle/article/365/18/fny200/5068690). Marketing di era digital saat ini sudah jauh melampaui dari sekedar mempresentasikannya di sebuah seminar. Suatu karya perlu dipasarkan dengan menawarkan: **akses, kelengkapan, dan keterikatan** antara peneliti/penulis dengan pembaca (lingkup sempit yaitu ekosistem riset dan lingkup luas yaktu masyarakat umum). Jadi kalaupun jumlah sitasi yang dikejar, ada tahapan lain yang saat ini tidak pernah dikenali dan diajarkan kepada para peneliti Indonesia. 

1. **Akses (Accessibility)**: saat ini aksesbilitas dijawab dengan penerbitan makalah dengan modus akses terbuka (_open access_/OA). Masalah berikutnya adalah persepsi salah yang mengidentikkan OA dengan biaya penerbitan (article processing cost/APC). Persepsi yang salah ini kemudian menciptakan pasar berikutnya yakni jurnal OA. Salah satu dampaknya adalah peneliti (dalam hal ini negara) butuh dana tambahan yang sangat besar di luar biaya riset agar bisa menerbitkan makalah secara OA. Secara cepat sebenarnya kita bisa melihat ini inefisiensi, tetapi itu tertutupi dengan persepsi reputasi yang didapatkan. Padahal mestinya tidak harus begitu. 
2. **Kelengkapan (Comprehensiveness)**: banyak yang merasa makalah atau artikelnya telah lengkap. Ada masalah, metode, data, dan analisis. Tapi ini tidak sepenuhnya benar. Seringkali kelengkapan tersebut tidak sampai pada level bahwa artikel atau riset tersebut dapat dibuat-ulang (*reproduce*) oleh orang lain. Bukankah reprodusibilitas (*reproducibility*) adalah salah satu prinsip utama sains? Saat ini dunia sedang [mengalami krisis itu](https://ecrcommunity.plos.org/2019/11/18/living-in-the-reproducibility-crisis/) (bacaan terkait lainnya [1](https://blogs.scientificamerican.com/observations/to-fix-the-reproducibility-crisis-rethink-how-we-do-experiments/), [2](https://www.nature.com/news/1-500-scientists-lift-the-lid-on-reproducibility-1.19970), [3](https://www.pnas.org/content/115/11/2628)). Salah satu yang dipromosikan untuk menangkal isu ini adalah data terbuka (*open data*).
3. **Keterikatan (Engagement)**: keterikatan yang saya maksud adalah hubungan antara penulis/peneliti dengan masyarakat yang menjadi pemangku kepentingan utama. Masyarakat adalah subyek yang merasakan masalah yang dipecahkan oleh para peneliti melalui risetnya. Untuk membina keterikatan dibutuhkan: **relevansi** antara riset dengan masalah masyarakat, **komunikasi** dengan berbagai kanal, serta **refleksi **ketika ditemukan ada kekurangan atau kesalahan dalam riset agar dapat diperbaiki. 

Jadi [budaya mengejar SEKEDAR sitasi yang mungkin diturunkan dari senior ke yunior](https://hackmd.io/@dasaptaerwin/SJV7ekPm8) harus segera dihapuskan, karena bertentangan dengan filosofi sains itu sendiri.   

## anotasi 2

> Menyusun rencana kerja dan anggaran; (halaman 3)

Artinya: Perlu ada kriteria tambahan untuk 

	1. meminimumkan anggaran negara yang berpotensi menguntungkan sektor privat secara sepihak, 
	2. menjaga kenetralan perguruan tinggi.
	
Untuk diketahui bersama, mayoritas penerbit ilmiah yang masuk ke dalam kategori-kategori yang diatur dalam pasal dan ayat di bawah, adalah penerbit komersial. Yang mereka tawarkan adalah penerbitan secara akses terbuka (open access/OA) [dengan biaya penerbitan berkisar antara ratusan USD sampai lebih dari USD 5000](https://elpub.episciences.org/4604/pdf) (The Lancet, jurnal kesehatan dan kedokteran). Sebuah studi juga pernah menghitung [kenaikan harga APC bisa mencapai 25% (2018-2019)](https://sustainingknowledgecommons.org/2019/11/26/apc-price-changes-2019-2018-by-journal-and-by-publisher/).

## anotasi 3

> laporan akuntabilitas kinerja (halaman 3)

Sangat berkaitan dengan anotasi nomor 2 di atas. Apapun yang tertulis dalam regulasi ini akan menjadi dasar untuk regulasi turunan yang mencerminkan akuntabilitas.

## anotasi 4

sengajar dikosongkan untuk "kriteria kelanjutan studi"

## anotasi 5

> .... sudah berpenghasilan lebih dari 1,2 (satu koma dua) kali UMR sebelum lulus, bekerja sebagai peran sebagaimana disebut pada huruf a) di atas. (halaman 2)

Kalau melihat pasal ini, maka lulusan PTN akademik dan vokasi yang menginisiasi (founding) suatu perusahaan dan belum memiliki penghasilan 1,2 x UMR tidak masuk perhitungan.

Apakah kriteria pendapatan dapat diaplikasikan untuk indikator kewiraswastaan. Bukankah kepeloporan yang mestinya diutamakan?

## anotasi 6

> ... membuka sanggar (halaman 2)

Sementara untuk lulusan PTN seni budaya tidak ada kriteria pendapatan.

## anotasi 7

> perguruan tinggi, baik di dalam negeri mupun di luar negeri yang setidaknya memiliki program studi yang terdaftar dalam QS10O berdasarkan bidang ilmu (QS100 by subject); (halaman 5)

Penggunaan QS sebagai salah kriteria, berpotensi mengganggu pencapaiak kriteria yang lain. Kriteria yang digunakan QS mayoritas tidak berkaitan dengan berbagai kriteria yang tercantum dalam regulasi ini. Dengan adanya persepsi di kalangan pimpinan perguruan tinggi bahwa peringkat internasional adalah segalanya, maka besar kemungkinan mereka hanya akan berkonsentrasi mencapai kriteria QS (atau yang sejenisnya), dibanding kriteria lain yang ada dalam regulasi ini.

## anotasi 8

> Kriteria pengalaman praktisi perusahaan multinasional; (halaman 5)

Sektor privat banyak dijadikan kriteria, maka perlu ada kriteria tambahan untuk menjamin netralitas perguruan tinggi di Indonesia ketika perusahaan tersebut bermasalah, misal: kebakaran hutan, pencemaran sungai.

## anotasi 9

> ... bekerja sebagai praktisi di dunia industri dalam 5 (lima) tahun terakhir (halaman 6)

Perlu ada kriteria lain yang menjaga agar tidak banyak dosen malah berpraktek di luar perguruan tingginya (karena dibayar oleh dunia industri/sektor privat).

## anotasi 10

> Kualifikasi akademik S3/S3 terapan dari perguruan tinggi dalam negeri atau luar negeri yang relevan dengan program studi. (Kualifikasi dosen, halaman 6)

Kata-kata “relevan” di sini sudah dipakai, maka kata-kata “linear” harus dihapus dalam regulasi lain yang setingkat dan yang lebih rendah. Seringkali kata-kata “relevan” masih diartikan/dipersepsikan sebagai linear.

## anotasi 11

> Sertifikasi dari perusahaan Fortune 500 (halaman 7)

Perlu ada kriteria tambahan agar tidak ada lebih banyak lagi komersialisasi pendidikan dan hasil riset yang dibiayai oleh negara (lihat anotasi di halaman awal).

## anotasi 12

> Berpengalaman Praktisi (halaman 7)

hati-hati konflik kepentingan yang mungkin timbul ketika ada masalah.

## anotasi 13

> lembaga global yang bereputasi (halaman 9)

Lembaga pengindeks global ini perlu didefinisikan ulang, karena telah terbukti mereka banyak biasnya (https://osf.io/preprints/socarxiv/qhvgr/)

## anotasi 14

> masyarakat (halaman 9)

Kalau melihat kotak ini, maka yang dimaksud dengan “masyarakat” adalah dosen dan peneliti, bukan masyarakat umum.

## anotasi 15

> penelitian dikutip lebih dari 10 (sepuluh) kali oleh peneliti lain (halaman 9)

Kalau melihat kotak ini, maka yang dimaksud dengan “masyarakat” adalah dosen dan peneliti, bukan masyarakat umum.

## anotasi 16

> buku berhasil diterbitkan dengan skala distribusi tingkat nasional. (halaman 9)

Kriteria ini hanya berlaku untuk buku cetak, sementara saat ini dunia digital terkoneksi via internet, batas negara (nasional vs internasional) sudah terhapus dengan sendirinya.

> Karya rujukan: buku saku (handbook) pedoman (guidelines), manual, buku teks (textbook), monograf, ensiklopedia, kamus. (halaman 9)

## anotasi 17

Harus disadari bersama bahwa bertransaksi dengan penerbit nasional/internasional khususnya yang bersifat komersial membutuhkan pengetahuan lebih dalam tentang transfer HKI.

Kenyataan yang terjadi, para dosen/peneliti mengejar penerbitan internasional dengan motivasi prestise saja. Pertimbangan komersialisasi dan transfer HKI tidak pernah menjadi yang utama.

## anotasi 18

> Studi kasus (halaman 10)

Perlu kriteria tambahan juga agar tidak terjadi konflik kepentingan, ketika sebuah karya digunakan oleh pihak lain, terutama oleh lembaga internasional.

## anotasi 19

> Produk fisik, digital, dan algoritme (termasuk prototipe) (halaman 10)

Di sini banyak disebut “paten”. Perlu jadi pemahaman bersama pula bahwa seringkali konsep paten bertentangan dengan riset yang dibiayai negara yang seharusnya ditujukan semaksimum mungkin untuk masyarakat umum.

Dalam proses pengurusannya, paten mewajibkan atau sekurang-kurangnya mengindikasikan pentingnya ketertutupan dalam proses riset.

## anotasi 20

> Kriteria Rekognisi Internasional (halaman 11)

Rekognisi internasional di sini masih sangat bergantung kepada mitra internasional. Seolah tanpa mitra internasional, maka suatu karya tidak direkognisi sebagai karya internasional atau tidak mungkin direkognisi secara internasional.

## anotasi 21

> Visual, audio, audio-visual, pertunjukan, performance) (halaman 11)

Kriteria “dibiayai atau diakuisisi oleh pihak lain”, terutama sektor privat ini rawan masalah juga. bagaimana bila ada karya seni yang diakuisisi perusahaan asing? (misal disain batik diakuisisi oleh perusahaan Malaysia)


> karya diakuisisi atau dibiayai oleh industri (halaman 12)
> Karya tulis novel, sajak, puisi, notasi musik (halaman 12)
> Karya preservasi, contoh: modernisasi seni tari daerah (halaman 13)

Di sini selain memperhatikan anotasi no 21, saya juga menyampaikan bahwa nilai HKI (hak kekayaan intelektual) jauh lebih penting dibanding komponen lainnya. Bercermin dari model publikasi jurnal ilmiah, maka seeorang pencipta harus hati-hati ketika ada peran serta dari pihak ketiga terutama dari entitas bisnis, karena dapat saja itu mensyaratkan pengalihan HKI.

## anotasi 22

> Kriteria mitra (halaman 14)

Lihat anotasi 2 dan anotasi lain yang menyebut konflik kepentingan. Konflik kepentingan harus dideklarasikan.

## anotasi 23

> Lembaga akreditasi internasional lainnya (halaman 17)

Karena sudah menyebut “merek”, maka harus lengkap. Salah satu merek yang belum disebut adalah: ASIIN.

> akreditasi atau sertifikasi institusi yang diberikan lembaga yang direkognisi dan bereputasi secara internasional (halaman 17)

Mestinya kriteria tidak menyebut merek.

Bila kita merujuk kepada arti kata “merdeka”, maka sebenarnya penggunaan lembaga akreditasi yang jelas-jelas tidak mendukung misi negara akan berujung kepada keterkungkungan, bukan kemerdekaan.

> Anotasi ini akan saya kembangkan lebih lanjut.